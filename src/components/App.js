import React, { Component } from 'react';
import { Switch, Route } from "react-router-dom";
import './App.css';
import Home from './Home';
import NewDesign from './NewDesign';

/**
 * App Component
 * @extends Component
 */
class App extends Component {
  state = {
    title: 'VideoCoin Dev Net',
    currentNodeId: null
  }

  onNodeSelected = (id) => {
    this.setState({
      currentNodeId: id
    });
  }

  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/new-design" component={NewDesign} />
      </Switch>
    );
  }
}

export default App;
